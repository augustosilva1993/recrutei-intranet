import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import HomeWorkOrder from '@/components/work-order/HomeWorkOrder'
import HomeWorkOrderHistory from '@/components/work-order-history/HomeWorkOrderHistory'
import WorkOrderForm from '@/components/work-order/WorkOrderForm'
import WorkOrderHistoryForm from '@/components/work-order-history/WorkOrderHistoryForm'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/work-order',
      name: 'HomeWorkOrder',
      component: HomeWorkOrder
    },
    {
      path: '/work-order/form',
      name: 'CreateWorkOrderForm',
      component: WorkOrderForm
    },     
    {
      path: '/work-order/form/:id',
      name: 'EditWorkOrderForm',
      component: WorkOrderForm
    },
    {
      path: '/work-order-history/:id',
      name: 'HomeWorkOrderHistory',
      component: HomeWorkOrderHistory
    },
    {
      path: '/work-order-history/form/:id',
      name: 'CreateWorkOrderHistoryForm',
      component: WorkOrderHistoryForm
    },     
  ]
})
