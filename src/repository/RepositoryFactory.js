import AuthRepository from './authRepository'
import QuizRepository from './quizRepository'

const repositories = {
  auth: AuthRepository,
  quiz: QuizRepository
}

export const RepositoryFactory = {
  get: name => repositories[name]
}
