import axios from 'axios'
import eventHub from '../util/eventHub'
import config from './../vue.config'

const baseDomain = config.API
const baseUrl = `${baseDomain}/api/v1`

const ax = axios.create({
  baseURL: baseUrl,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest'
  }
})

export default ax

ax.interceptors.request.use(
  conf => {
    eventHub.$emit('before-request')
    return conf
  },
  error => {
    eventHub.$emit('request-error')
    return Promise.reject(error)
  }
)

ax.interceptors.response.use(
  response => {
    eventHub.$emit('after-response')
    return response
  },
  error => {
    eventHub.$emit('response-error')
    return Promise.reject(error)
  }
)

ax.interceptors.response.use(
  response => {
    try {
      if (response.config.method === 'post' || response.config.method === 'put' || response.config.method === 'delete') {
        if (!response.data.error) {
          eventHub.$emit('success-handler', response.data)
        } else {
          eventHub.$emit('error-handler', response.data.message)
        }
      }
    } catch (err) {
      eventHub.$emit('error-handler', err)
    }
    return response
  },
  error => {
    eventHub.$emit('error-handler', error.response)
    alert(error.message)
    return Promise.reject(error)
  }
)
