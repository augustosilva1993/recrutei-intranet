import Repository from './Repository'

const resource = '/work-order-history'

export default {
  list () {
    return Repository.get(`${resource}`, { headers: {
      'access-role': localStorage.profile,
    }})
  },
  getOne (idWorkOrder) {
    return Repository.get(`${resource}/${idWorkOrder}`, { headers: {
      'access-role': localStorage.profile,
    }})
  },
  save (workOrder) {
    return Repository.post(`${resource}`, workOrder, { headers: {
      'access-role': localStorage.profile,
    }})
  },
  update (id, workOrder) {
    return Repository.put(`${resource}/${id}`, workOrder, { headers: {
      'access-role': localStorage.profile,
    }})
  },
  remove (id) {
    return Repository.delete(`${resource}/${id}`, { headers: {
      'access-role': localStorage.profile,
    }})
  }
}