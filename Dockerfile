# build stage
FROM node:9.11.1-alpine AS build-stage
LABEL MAINTAINER Augusto Silva <augusto.t.silva1993@gmail.com>
WORKDIR /
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# production stage
FROM nginx:1.13.12-alpine AS production-stage
COPY --from=build-stage /dist /usr/share/nginx/html
COPY --from=build-stage /nginx/default.conf /etc/nginx/conf.d
EXPOSE 81
CMD ["nginx", "-g", "daemon off;"]