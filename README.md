# Recrutei Intranet

## Descrição da aplicação

Este serviço tem como finalidade manter os registros de chamados do cliente abertos pelo Service Desk.

* Um usuário com perfil "Service-Desk" deve ter permissão para criar uma Ordem de Serviço (OS) e consultar suas alterações.

* Um usuário com perfil "Developer" deve ter permissão para adicionar "notas" (OS Histórico) a OS.

* Um usuário com perfil "Admin" deve ter permissão para editar e excluir OS e Histórico.

## Tecnologias

A aplicação foi desenvolvida utilizando VueJS (ES6) e conta com o Docker para a geração da imagens e rápida implantação.

### CI/CD do Bitbucket

Criação de pipeline no Bitbucket integrada ao Docker HUB para geração de imagens. Pode-se aprofundar e implementar a execução de testes unitários, validação do Dockerfile e entrega em diferentes ambientes após aprovações.


## Utilização

### Docker

A imagem do docker contendo a aplicação está disponível em LINK DOCKER

Para rodar é necessário executar os comandos:

`docker pull augustotsilva1993 recrutei-intranet:ce8fb7f6f5626a3048d86451afb8d30551c95cd5`

`docker run -it -p 8081:81 --rm --name recrutei-intranet augustotsilva1993/recrutei-intranet:ce8fb7f6f5626a3048d86451afb8d30551c95cd5`

A aplicação estará disponível na porta 8081.

### Projeto

O projeto pode ser clonado do repositório e executado com os comandos

`npm install` 

e 

`npm run build`

As configurações podem ser alteradas no .env